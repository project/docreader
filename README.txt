/* $Id$ */

docReader provides an formatter to the CCK module Filefield (for Drupal 6) or the core file module in Drupal 7.

When enabled and used as a file formatter, this module outputs a second link next the normal file link, that you normally would see in filefield. The second link goto the service provided by Readspeaker "docReader". When you click on the second file link, a new page opens and the service read the text for you with a computerized/recorded voice in a vareity of languages. You will need an account at Readspeaker to use this service. The avaiable document formats that the service can read from are: pdf, doc, docx, rtf, odt.


Dependencies:

 * Content
 * Filefield


Install:

1) Copy the docreader folder to the modules folder in your installation.

2) Enable the module by going to "Modules"
   (/admin/modules).

3) Create a new file field in a content type, then click manage display on the type you want to change the file formatter. 

4) Select "docReader file" in the select lists for that field.

5) Goto Site configuration > docReader settings and choose voice language and provide your docReader ID.

6) Upload files on the node form for the type that you set up.
